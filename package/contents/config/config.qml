//tady sou definovaný taby v konfiguraci widgetu naploše
import QtQuick 2.0

import org.kde.plasma.configuration 2.0 as PlasmaConfig

PlasmaConfig.ConfigModel
{
    PlasmaConfig.ConfigCategory
    {
        name: i18n('Nastavení')
        icon: 'preferences-desktop-plasma'
        source: 'nastaveni.qml'
    }
    
    PlasmaConfig.ConfigCategory
    {
        name: i18n('Specialitky')
        icon: 'preferences-desktop-plasma'
        source: 'nastaveniExtra.qml'
    }

}

 
