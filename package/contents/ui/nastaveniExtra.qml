import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls 1 as StarodavnyQtControls
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout
{
    id: page
    Kirigami.FormData.label: i18n('Specialitky')

    property alias cfg_rezim: konfiguraceRezim.checked
    
    CheckBox
    {
        id: konfiguraceRezim
        Kirigami.FormData.label: i18n('Režim Milana Uhráka:')
    }
}
 
