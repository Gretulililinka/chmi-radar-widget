import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls 1 as StarodavnyQtControls
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout
{
    id: page
    Kirigami.FormData.label: i18n('Nastavení')
    
    // proměný musej mit ten prefix 'cfg_' aby si to jakože spárovalo s vodpovídajícíma hodnotama v souboru config.xml 
    property alias cfg_rychlostAnimace: konfiguraceRychlostAnimace.value
    property alias cfg_refreshInterval: konfiguraceRefreshInterval.value
    property alias cfg_budoucnost: konfiguraceBudoucnost.checked
    property alias cfg_kolikDoMinulosti: konfiguraceKolikDoMinulosti.value
    
    // zobrazení mapky
    property alias cfg_druhMapky: konfiguraceDruhMapky.currentIndex
    property alias cfg_legenda: konfiguraceLegenda.checked
    property alias cfg_hranice: konfiguraceHranice.checked
    
    

        
    CheckBox
    {
        id: konfiguraceBudoucnost
        Kirigami.FormData.label: i18n('ukazovat taky předpověď:')
    }
    
    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceKolikDoMinulosti
        decimals: 0
        minimumValue: 1
        Kirigami.FormData.label: i18n('kolik vobrázků z minulosti ukazovat (voni maj 7):')
    }
    
    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRychlostAnimace
        decimals: 2
        stepSize: 0.1
        minimumValue: 0.1
        suffix: 'x'
        Kirigami.FormData.label: i18n('rychlost přehrávání animace:')
    }

    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRefreshInterval
        decimals: 0
        minimumValue: 1
        suffix: ' minut'
        Kirigami.FormData.label: i18n('refreshovávací interval:')
    }
    
    ComboBox
    {
        id: konfiguraceDruhMapky
        model: ['Originální', 'Invertovaná']
        Kirigami.FormData.label: i18n('druh mapky:')
    }
    
    CheckBox
    {
        id: konfiguraceLegenda
        Kirigami.FormData.label: i18n('ukazovat legendu (města/cesty/etc):')
    }
    
    CheckBox
    {
        id: konfiguraceHranice
        Kirigami.FormData.label: i18n('ukazovat státní hranice:')
    }
}
 
