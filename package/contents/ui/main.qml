import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents

Item
{
    
    Plasmoid.fullRepresentation: Item
    {
        id: main
        
        property int refreshInterval: plasmoid.configuration.refreshInterval
        property real rychlostAnimace: plasmoid.configuration.rychlostAnimace
        property bool budoucnost: plasmoid.configuration.budoucnost
        property real kolikDoMinulosti: plasmoid.configuration.kolikDoMinulosti
        
        // mapa
        property int druhMapky: plasmoid.configuration.druhMapky
        property bool ukazovatLegendu: plasmoid.configuration.legenda
        property bool ukazovatHranice: plasmoid.configuration.hranice
        
        //specialitky
        //režim milana uhráka
        property bool rezim: plasmoid.configuration.rezim
        
        Layout.minimumWidth: 340 * PlasmaCore.Units.devicePixelRatio
        Layout.minimumHeight: 230 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredWidth: 680 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredHeight: 460 * PlasmaCore.Units.devicePixelRatio
        
        function mapa()
        {
            switch(druhMapky)
            {
                case 0: return 'republiky/orig_relief.jpg';
                case 1: return 'republiky/invertovana_relief.jpg';
                default: return 'republiky/orig_relief.jpg';
            }
        }
        
        Image
        {
            id: pozadiMapy
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent;
            source: mapa()
            
        }
        Image
        {
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent;
            source: 'republiky/orig_mesta.png'
            visible: ukazovatLegendu
        }
        
        
        // sem budou nastrkaný vobrázky animace
        Rectangle
        {
            id: kontik
            width: parent.width
            height: parent.height
            color: '#00000000'
        }
        
        Image
        {
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent;
            source: 'republiky/orig_okraje.png'
            visible: ukazovatHranice
            
        }
        
        // pokusy vodstranit takovej ten šedivej roh nahoře pomocí shaderu
        // boužel nechává artefakty tak je vyplej
        // btw si všiměte že chmi.cz na tom místě hnusnýho šedivýho rohu má gui by to nebylo vidět :D
        /*
        ShaderEffectSource
        {
            id: shaderSrc
            sourceItem: kontik
        }
        
        ShaderEffect {
            width: parent.width
            height: parent.height
            property variant src: shaderSrc
            property variant bckground: pozadiMapy
            vertexShader: "
                uniform highp mat4 qt_Matrix;
                attribute highp vec4 qt_Vertex;
                attribute highp vec2 qt_MultiTexCoord0;
                varying lowp vec2 coord;
                void main() {
                    coord = qt_MultiTexCoord0;
                    gl_Position = qt_Matrix * qt_Vertex;
                }"
            fragmentShader: "
                varying highp vec2 coord;
                uniform sampler2D src;
                uniform sampler2D bckground;
                uniform lowp float qt_Opacity;
                
                void main() {
                    lowp vec4 tex = texture2D(src, coord);
                    lowp vec4 tex2 = texture2D(bckground, coord);
                    //gl_FragColor = tex.rgba;
                    //gl_FragColor = vec4(mix(tex.rgb, tex2.rgb, 1.0-tex.a),1.0);
                    //gl_FragColor = vec4(vec3(1.0,1.0,1.0) - tex.rgb, tex.a);
                    gl_FragColor = mix(tex2, tex, tex.a);
                    
                    //lowp vec4 tex2 = texture2D(bckground, coord);
                    
                    gl_FragColor = (tex2.rgba * (1.0-tex.a)) + (tex.rgba * tex.a);
                    if(tex.a < 0.99 && tex.r > 0.7 && tex.g > 0.7 && tex.b > 0.7)
                        gl_FragColor=tex2.rgba;
                    //nefunguje dobře :(
                    if(length(abs(gl_FragColor.rgb - vec3(0.76171875,0.76171875,0.76171875))) < 0.1)
                    {
                        lowp vec4 tex2 = texture2D(bckground, coord);
                        gl_FragColor.rgba = tex2.rgba;
                    }

                }"
        }
        */

        
        property var obrazky: []
        property int index: 0 //index právě zobrazenýho obrázku
        
        // časovač na přehrávání animace
        // ne všechny obrázky sou stažený a ne všecky de postahovat
        // když třeba teprve počitaj modýlek tak tam ty vobrázky eště nejsou na stažení
        // a qml image element kterej bude mit takovej obrázek jako source nebude možný zobrazit tak ho
        // jakoby musíme přeskočit
        Timer
        {
            id: animaceTimer
            interval: 1000 * (1.0/rychlostAnimace)
            repeat: true
            running: !rezim
            onTriggered:
                {
                                        
                    if (obrazky !== undefined && obrazky.length > 1)
                    {
                    
                        //if(obrazky[index] !== undefined)
                        obrazky[index].visible = false;
                        
                        for(let i = 0; i < obrazky.length; i++)
                        {
                            index++;
                            if(index == obrazky.length)
                            {
                                index = 0;
                            }
                            
                            // ukazujem jenom obrázky který sou ready -> tzn. už stažený a připravený
                            if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                            {
                                obrazky[index].visible = true;
                                break;
                            }

                        }
                    }
                    else if(obrazky.length == 1)
                    {
                        if(obrazky[0] !== undefined && obrazky[0].status == Image.Ready)
                        {
                            obrazky[0].visible = true;
                        }
                        
                    }

                }
        }
        
        // časovač na stahování obrázků
        Timer
        {
            interval: 1000 * 60 * refreshInterval
            repeat: true
            running: true
            onTriggered: vyrobitObrazky()
        }
        
        Timer
        {
            id: uhrorezimTimer
            repeat: true
            running: rezim && !rezimCudlik.macknutej
            onTriggered:
            {
                if(obrazky[9].status == Image.Ready)
                    obrazky[9].visible=true;
            }
        }
        
        Component.onCompleted:
        {
            vyrobitObrazky();
        }
        
        onBudoucnostChanged:
        {
            vyrobitObrazky();
            animaceTimer.restart();
            index = 0;
        }
        
        onRezimChanged:
        {
            vyrobitObrazky();
            animaceTimer.restart();
            index = 0;
        }

        Button
        {
            id: rezimCudlik
            
            property bool macknutej: false
            width: pozadiMapy.paintedWidth / 8
            height: pozadiMapy.paintedHeight * 0.178
            x: (parent.width - pozadiMapy.paintedWidth)/2 + (pozadiMapy.paintedWidth - width)
            y: (parent.height - pozadiMapy.paintedHeight)/2
            contentItem: PlasmaCore.IconItem
            {
                source: rezimCudlik.macknutej ? "media-playback-pause" : "weather-storm"
            }
            padding: 0
            visible: rezim
            onClicked:
                {
                    
                    if(macknutej)
                    {
                        animaceTimer.stop();
                        zneviditelnitObrazky();
                        if(obrazky[9].status == Image.Ready)
                            obrazky[9].visible=true;
                        
                    }
                    else
                    {

                        vyrobitObrazky();
                        if(obrazky[9].status == Image.Ready)
                            obrazky[9].visible=false;
                        index = 0;
                        animaceTimer.restart();
                    }
                    
                    macknutej = !macknutej;
                    
                }
        }

            
        function zneviditelnitObrazky()
        {
            for(let i = 0; i < obrazky.length; i++)
            {
                if(obrazky[i] !== undefined && obrazky[i].status == Image.Ready)
                {
                    obrazky[i].visible = false;
                }
            }
        }
            
        // funkce která naplní kontik vybranejma vobrázkama
        // vyrábí je jako dynamický voběkty Image který maj ten danej vobrázek jako url co vede na chmi.cz
        function vyrobitObrazky()
        {
            console.log('chmi.cz radar widget: stahujou se vobrázky');
            var date = new Date();

            //snimek maj za každejch 10 minut
            // maj tam +- 7 obrázků dycky ukázanejch ale relalně jich tam maj skovanejch vo moc víc
            // (ukazovat starý vobrázky pro ně asi jako nemá víznam žádnej)
            var minuty = date.getUTCMinutes();
            var kolikObrazku = rezim ? 10 : kolikDoMinulosti;
            date.setUTCMinutes(minuty - (minuty % 10) - (10 * kolikObrazku));
            
            if(obrazky.length > 0)
            {
                for(let i=0;i<obrazky.length;i++)
                {
                    obrazky[i].destroy();
                }
                obrazky = [];
            }
            
            
            var obektString = 'import QtQuick 2.11;';
                obektString += 'Image{';
                obektString += 'fillMode: Image.PreserveAspectFit;';
                obektString += 'anchors.fill: parent;';
                obektString += 'visible: false;';
                
            for(let i=0;i<kolikObrazku;i++)
            {
                date.setUTCMinutes(date.getUTCMinutes()+10);
                // obrázky skovávaj do složšky která se menuje podle aktuálního UTC datumu 
                var url = date.getUTCFullYear() + ("0"+(date.getUTCMonth()+1)).slice(-2) + ("0"+(date.getUTCDate())).slice(-2) + "." + ("0"+(date.getUTCHours())).slice(-2) + ("0"+(date.getUTCMinutes())).slice(-2);
                
                url = 'https://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d_masked/pacz2gmaps3.z_max3d.'+url+'.0.png';
                
                console.log(url);
                        

                var objstr = obektString + 'source: \"' + url +'\";';
                objstr += '}';
                        
                var obekt = Qt.createQmlObject(objstr, kontik , 'obrazek_'+i);
                obrazky.push(obekt);
            }
            
            
            if(budoucnost || rezim)
            {
                // přepověď z hejbání mraků asi
                // za posledním vobrázkem maj na hodinu dopředu zase s 10timinutovýma skokama
                
                // něco jako
                // https://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d_fct_masked/20220729.0930/pacz2gmaps3.fct_z_max.20220729.1020.50.png
                var prefix_url = 'https://www.chmi.cz/files/portal/docs/meteo/rad/inca-cz/data/czrad-z_max3d_fct_masked/'
                prefix_url += date.getUTCFullYear() + ("0"+(date.getUTCMonth()+1)).slice(-2) + ("0"+(date.getUTCDate())).slice(-2) + "." + ("0"+(date.getUTCHours())).slice(-2) + ("0"+(date.getUTCMinutes())).slice(-2);
                prefix_url += '/pacz2gmaps3.fct_z_max.'
                
                for(let i=1;i<7;i++)
                {
                    date.setUTCMinutes(date.getUTCMinutes()+10);
                    var url = date.getUTCFullYear() + ("0"+(date.getUTCMonth()+1)).slice(-2) + ("0"+(date.getUTCDate())).slice(-2) + "." + ("0"+(date.getUTCHours())).slice(-2) + ("0"+(date.getUTCMinutes())).slice(-2);
                    
                    url = prefix_url+url+'.'+(i*10)+'.png';
                    var objstr = obektString + 'source: \"' + url +'\";';
                    objstr += '}';
                            
                    var obekt = Qt.createQmlObject(objstr, kontik , 'obrazek_'+i);
                    obrazky.push(obekt);
                }
            
            }
            
                    
            // pokud to pude zkusíme zobrazit první vobrázek
            if(rezim)
            {                
                if(obrazky[9].status == Image.Ready)
                    obrazky[9].visible=true;
            }
            else
            {
                if(obrazky[0].status == Image.Ready)
                    obrazky[0].visible=true;
            }
                    

        }
    }
}
